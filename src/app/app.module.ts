import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { ChatPage } from "../pages/chat/chat";
import { AddUserPage } from "../pages/add-user/add-user";
import { LoginPage } from "../pages/login/login";
import { RegisterPage } from "../pages/register/register";
import { AlertProvider } from '../providers/alert/alert';
import { ChatProvider } from "../providers/chat-provider";
import { Geolocation } from '@ionic-native/geolocation';


export const firebaseConfig = {
    apiKey: "AIzaSyBhcZrxAY-9_Zcs3Zu3LoB_kM_8-eK51Ho",
    authDomain: "klindic-chat-app.firebaseapp.com",
    databaseURL: "https://klindic-chat-app.firebaseio.com",
    projectId: "klindic-chat-app",
    storageBucket: "",
    messagingSenderId: "683089531111"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ChatPage,
    AddUserPage,
    LoginPage,
    RegisterPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ChatPage,
    AddUserPage,
    LoginPage,
    RegisterPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AlertProvider,
    ChatProvider,
    Geolocation
  ]
})
export class AppModule {}
