import { User } from "./user";
import { Message } from "./message";

export class ChatInfo {
    chatKey: string;
    uid: string;
    friend: User;
    messages: Message[];
}