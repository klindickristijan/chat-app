import { User } from "./user";

export class LastMessage {
    friend: User;
    text: string;
    timestamp: string;
}