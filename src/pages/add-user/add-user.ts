import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, AlertController } from 'ionic-angular';
import { FirebaseListObservable, AngularFireDatabase, FirebaseObjectObservable } from "angularfire2/database";
import { HomePage } from "../home/home";
import { User } from "../../classes/user";
import { AngularFireAuth } from "angularfire2/auth";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/mergeMap';
import { ChatPage } from "../chat/chat";

@Component({
  selector: 'page-add-user',
  templateUrl: 'add-user.html',
})
export class AddUserPage {
// private friends = new Array<User>();
// private allUsers = new Array<User>();
private nonFriends = new Array<User>();

private currentUserId = this.fireAuth.auth.currentUser.uid;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private database: AngularFireDatabase,
    private fireAuth: AngularFireAuth,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController
  ) {

    this.getNonFriends(this.currentUserId)
      .subscribe(nonFriends => this.nonFriends = nonFriends);
  }

  getNonFriends(currentUserId: string): Observable<User[]> {
    return this.database
      .list('friends/' + currentUserId)
      .map(friends => friends.map(friend => friend.$key))
      .flatMap(friendIds => {
        friendIds.push(currentUserId);
        return this.database
        .list('users')
        .map(users => {
          return users
            .filter(user => friendIds.indexOf(user.$key) === -1)
            .map(user => {
              return {
                uid: user.$key,
                firstName: user.firstName,
                lastName: user.lastName
              }
            })
        })
      })
  }

  addUser(nonFriend: User): void {

    let alert = this.alertCtrl.create({
        title: 'Add ' + nonFriend.firstName + ' ' + nonFriend.lastName,
        message: 'Are you sure you want to add ' + nonFriend.firstName + ' ' + nonFriend.lastName + '?',
        buttons: [
          {
              text: 'No!',
              role: 'cancel',
              handler: () => {
              }
          },
          {
              text: 'Yes!',
              handler: () => {
                this.database.object('friends/' + this.currentUserId + '/' + nonFriend.uid)
                .set({
                  chat: true
                });
                this.database.object('friends/' + nonFriend.uid + '/' + this.currentUserId)
                .set({
                  chat: true
                });
                this.navCtrl.pop();
                this.navCtrl.push( ChatPage, { friend: nonFriend });
              }
          }
        ]
    });
    alert.present();

  }

  searchNonFriends() {
    document.getElementById('hiddenInput').style.visibility = 'visible';
    
  }

}
