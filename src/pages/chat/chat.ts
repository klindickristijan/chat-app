import { Component, OnDestroy, OnInit, ViewChild, Injectable } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, List } from 'ionic-angular';
import { AngularFireDatabase, FirebaseListObservable } from "angularfire2/database";
import { User } from "../../classes/user";
import { Subject } from "rxjs/Subject";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AlertProvider } from "../../providers/alert/alert";
import { Message } from "../../classes/message";
import { ChatInfo } from "../../classes/chat-info";
import { ChatProvider } from "../../providers/chat-provider";
import { AngularFireAuth } from "angularfire2/auth";
import { Observable } from "rxjs/Observable";
import moment from 'moment';
import { Geolocation } from '@ionic-native/geolocation';
import googleMapsGeocoder from 'google-maps-geocoder';

@Injectable()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage implements OnDestroy, OnInit{
  @ViewChild(Content) content: Content;
  @ViewChild(List) list: List;

  private ngUnsubscribe: Subject<void> = new Subject<void>();
  private friend: User;
  private chatInfo: ChatInfo;
  private messageForm: FormGroup;
  private msgs = new Array<Message>();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertData: AlertProvider,
    public chatData: ChatProvider,
    public formBuilder: FormBuilder,
    public database: AngularFireDatabase,
    private fireAuth: AngularFireAuth,
    private geolocation: Geolocation
  ) {
    this.friend = this.navParams.get('friend');
    this.messageForm = this.formBuilder.group({
      text: ['', Validators.required]
    });

    this.getMessages()
      .subscribe(messages => { this.msgs = messages })

  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  ngOnInit(): void {
        // Move scroll to bottom on each new message.
        this.database.database
          .ref('messages/' + this.fireAuth.auth.currentUser.uid + '/' + this.friend.uid)
          .limitToLast(1)
          .on('child_added', snapshot => {
            this.scrollToBottom(1000);
        });
}

  ionViewDidEnter(): void {
    this.scrollToBottom();
  }

  scrollToBottom(duration: number = 0): void {
    try {
      this.content.scrollTo(0, this.list._elementRef.nativeElement.offsetHeight, duration);
    } catch (error) {}
  }

  sendMessage(): void {
    if (!this.messageForm.valid) {
      return;
    }

    let location: string;
    location = ' ';

    this.geolocation.getCurrentPosition().then((resp) => {
      let lat = resp.coords.latitude;
      let lng = resp.coords.longitude;
      location = lat + ' ' + lng;
      const geocoder = googleMapsGeocoder('AIzaSyAEPocpckC4xu9UuvzS3qE3z6bnhwVyocU');
      geocoder.reverseGeocode(lat, lng).then(where => { 
        
        location = where;
      
          let message = new Message();
          message.uid = this.friend.uid;
          message.text = this.messageForm.value.text;
          message.timestamp = Date.now().toString();
          message.send = true;
          message.location = location;
      
          this.chatData
            .addMessage(this.fireAuth.auth.currentUser.uid, this.friend.uid, message)
            .then(
              () => this.messageForm.reset(),
              error => error);
              
            message = new Message();
            message.uid = this.fireAuth.auth.currentUser.uid;
            message.text = this.messageForm.value.text;
            message.timestamp = Date.now().toString();
            message.send = false;
            message.location = location;
      
          this.chatData
            .addMessage(this.friend.uid, this.fireAuth.auth.currentUser.uid, message)
            .then(
              () => this.messageForm.reset(),
              error => error);
            });
     }).catch((error) => {
       console.log('Error getting location', error);
     });
  }

  getMessages(): Observable<Message[]> {
    return this.database
      .list('messages/' + this.fireAuth.auth.currentUser.uid + '/' + this.friend.uid)
      .map(messages => {
        let allMessages = new Array<Message>();

        messages.forEach(message => {
              allMessages.push({
                uid: message.uid,
                text: message.text,
                timestamp: message.timestamp,
                send: message.send,
                location: message.location
              });
          });
        return allMessages;
      });
  }

  getMessageTime(msg: Message){
      let date = new Date(Number(msg.timestamp));
      let formattedDate = moment(date).format('DD.MM.YY. HH:mm:ss');
      return formattedDate;
  }
}
