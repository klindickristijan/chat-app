import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, Loading, LoadingController, AlertController } from 'ionic-angular';
import { AngularFireDatabase } from "angularfire2/database";
import { ChatPage } from '../chat/chat';
import { AddUserPage } from "../add-user/add-user";
import { AngularFireAuth } from "angularfire2/auth";
import { LoginPage } from "../login/login";
import { User } from "../../classes/user";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/map';
import { Message } from "../../classes/message";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private loading: Loading;
  private loadingCtrl: LoadingController;
  private user: User = { uid: '', firstName: '', lastName: '' };
  private friends = new Array<User>();
  private currentUserId = this.fireAuth.auth.currentUser.uid;
  private chat: ChatPage;
  private msgs = new Array<Message>();

  constructor(
    private navCtrl: NavController,
    private database: AngularFireDatabase,
    private navParams: NavParams,
    private fireAuth: AngularFireAuth,
    private alertCtrl: AlertController
  ) {
    // Displaying name on Toolbar
    this.getCurrentUser(this.currentUserId)
      .subscribe(user => this.user = user)

    // Displaying all friends
    this.getFriends(this.currentUserId)
      .subscribe(friends => this.friends = friends)
  }

  // getLastMessage() {
  //   for (var i = 0; i < this.friends.length; i++) {
  //     return this.database
  //       .object('messages/' + this.currentUserId + '/' + this.friends[i].uid)
  //       .map(messages => {
  //         return {
  //           uid: messages.uid,
  //           text: messages.text,
  //           timestamp: messages.timestamp,
  //           send: messages.send
  //         }
  //       })
  //   }
  // }

  getCurrentUser(currentUserId: string): Observable<User> {
    return this.database
      .object('users/' + currentUserId)
      .map(user => {
        return {
          uid: user.$key,
          firstName: user.firstName,
          lastName: user.lastName
        };
      });
  }

  getFriends(currentUserId: string): Observable<User[]> {
    return this.database
      .list('friends/' + currentUserId)
      .map(friends => {
        let allFriends = new Array<User>();

        friends.forEach(friend => {
          this.database
            .object('users/' + friend.$key)
            .subscribe(user => {
              allFriends.push({
                uid: user.$key,
                firstName: user.firstName,
                lastName: user.lastName
              });
            });
        });

        return allFriends;
      });
      
  }

  openChat( friend: User ) {
    this.navCtrl.push( ChatPage, { friend: friend });
  }

  addPage() {
    this.navCtrl.push(AddUserPage);
  }

  deleteFriend( friend: User ) {

    let alert = this.alertCtrl.create({
        title: 'Delete ' + friend.firstName + ' ' + friend.lastName,
        message: 'Are you sure you want to delete ' + friend.firstName + ' ' + friend.lastName + '?',
        buttons: [
          {
              text: 'No!',
              role: 'cancel',
              handler: () => {
              }
          },
          {
              text: 'Yes!',
              handler: () => {
                this.database.list('friends/' + this.currentUserId + '/' + friend.uid).remove();
                this.database.list('messages/' + this.currentUserId + '/' + friend.uid).remove();
                this.database.list('friends/' + friend.uid + '/' + this.currentUserId).remove();
                this.database.list('messages/' + friend.uid + '/' + this.currentUserId).remove();
              }
          }
        ]
    });
    alert.present();
  }

  logout() {

    let alert = this.alertCtrl.create({
        title: 'Logout!',
        message: 'Are you sure you want to log out? ',
        buttons: [
          {
              text: 'No!',
              role: 'cancel',
              handler: () => {
              }
          },
          {
              text: 'Yes!',
              handler: () => {
                this.fireAuth.auth.signOut();
                this.navCtrl.setRoot(LoginPage);
              }
          }
        ]
    });
    alert.present();
  }

}