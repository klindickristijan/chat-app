import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Loading, LoadingController } from 'ionic-angular';
import { AngularFireAuth } from "angularfire2/auth";
import { HomePage } from "../home/home";
import { RegisterPage } from "../register/register";
import { LoginUser } from "../../user/loginUser";
import { FirebaseListObservable, AngularFireDatabase, FirebaseObjectObservable } from "angularfire2/database";
import { AlertProvider } from "../../providers/alert/alert";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  private loading: Loading;

  private newUser: FirebaseObjectObservable<any>;
  private nonFriends;
  private curUserid = '';
  public user = { email: '', password: '' };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private fireAuth: AngularFireAuth,
    private database: AngularFireDatabase,
    private alertProvider: AlertProvider,
    private loadingCtrl: LoadingController
  ) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login(user: LoginUser): void {
    user.email = user.email.trim();
    this.fireAuth.auth.signInWithEmailAndPassword(user.email, user.password)
      .then(
        response => {
          this.navCtrl.setRoot(HomePage);
          this.presentLoadingDefault();
        },
        error => {
          this.alertProvider.showAlert(error.name, error.message);
        });
  }
  
  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Loading...',
      duration: 300
    });

    loading.present();
  }

  goToRegister() {
    this.navCtrl.push(RegisterPage);
  }
}