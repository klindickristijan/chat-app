import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase, FirebaseListObservable } from "angularfire2/database";
import { UserRegistration } from "../../classes/user-registration";
import { AlertProvider } from "../../providers/alert/alert";

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  private userRegistration: UserRegistration = new UserRegistration();
  private passwordConfirm: String;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private fireAuth: AngularFireAuth,
              private database: AngularFireDatabase,
              private alertProvider: AlertProvider,
              private alertCtrl: AlertController
              ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  register(pass1, pass2) {

    if (pass1 !== pass2) {
      
      let alert1 = this.alertCtrl.create({
        title: "Error",
        subTitle: 'Your "password confirm" does not match "password"!',
        buttons: ['OK']
      });
      alert1.present();
    } else {

      let alert2 = this.alertCtrl.create({
          title: 'Register',
          message: 'Are you sure you want to register?',
          buttons: [
            {
                text: 'No!',
                role: 'cancel',
                handler: () => {
                }
            },
            {
                text: 'Yes!',
                handler: () => {

                  
                  this.userRegistration.email = this.userRegistration.email.trim();
                  this.userRegistration.firstName = this.userRegistration.firstName.trim();
                  this.userRegistration.lastName = this.userRegistration.lastName.trim();


                  this.fireAuth.auth.createUserWithEmailAndPassword(this.userRegistration.email, this.userRegistration.password)
                    .then(user => {
                      this.database.object('/users/' + user.uid)
                        .set({
                          firstName: this.userRegistration.firstName,
                          lastName: this.userRegistration.lastName
                        });

                          let alert4 = this.alertCtrl.create({
                            title: "Sucsess!",
                            subTitle: 'You registered sucssesfully.',
                            buttons: ['OK']
                          });
                        alert4.present();
                        this.navCtrl.pop();
                    }, error => {
                      this.alertProvider.showAlert(error.name, error.message);
                    });
                }
            }
          ]
      });
      alert2.present();
    }
  }


}
