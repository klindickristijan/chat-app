import { AlertController } from 'ionic-angular';

export class AlertConfirm {

constructor(
    private alertCtrl: AlertController
    ) {}

presentConfirm() {
    let alert = this.alertCtrl.create({
        title: 'Add user',
        message: 'Are you sure you want to add',
        buttons: [
        {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
            console.log('Cancel clicked');
            }
        },
        {
            text: 'Buy',
            handler: () => {
            console.log('Buy clicked');
            }
        }
        ]
    });
    alert.present();
    }
}