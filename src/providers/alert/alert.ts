import { Injectable } from '@angular/core';
import { AlertController } from "ionic-angular";

@Injectable()
export class AlertProvider {

  constructor(private alertCtrl: AlertController) {
  }

  showAlert(title: string, subTitle: string): void {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subTitle,
      buttons: ['OK']
    });
    alert.present();
  }
}
