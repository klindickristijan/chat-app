import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";

import { AngularFireDatabase } from "angularfire2/database";
import firebase from 'firebase/app';

import { AngularFireAuth } from "angularfire2/auth";
import { Message } from "../classes/message";
import { User } from "../classes/user";
import { ChatInfo } from "../classes/chat-info";

@Injectable()
export class ChatProvider {
  
  private fireAuth: AngularFireAuth;
  private database: AngularFireDatabase;

  constructor(
    fireAuth: AngularFireAuth,
    database: AngularFireDatabase
  ) {
    this.fireAuth = fireAuth;
    this.database = database;
  }

  addMessage(uid1: string, uid2: string, message: Message): firebase.Promise<void> {
    return this.database
      .list('messages/' + uid1 + '/' + uid2)
      .push(message);
  }
}